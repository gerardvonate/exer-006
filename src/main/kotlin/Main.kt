package org.example

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File

data class Entry(
    val id: String,
    val email: String,
    val gender: String,
    @JsonProperty("first_name")
    val firstName: String,
    @JsonProperty("last_name")
    val lastName: String,
    @JsonProperty("ip_address")
    val ipAddress: String,
    @JsonProperty("favorite_animal")
    val favoriteAnimal: String,
    @JsonProperty("favorite_car_brand")
    val favoriteCarBrand: String,
    @JsonProperty("favorite_color")
    val favoriteColor: String,
    @JsonProperty("favorite_drug")
    val favoriteDrug: String,
    val race: String,
    val country: String,
)

val PROJECT_PATH = System.getProperty("user.dir")

fun main(args: Array<String>) {
    val mockDataPath = "$PROJECT_PATH/src/main/resources/mock-json-data"
    val jsonFiles = File(mockDataPath)
        .walk()
        .toList()
        .filter { file -> file.isFile && file.extension == "json" }

    val mapper = jacksonObjectMapper()

    val finalJson = File("$PROJECT_PATH/src/main/resources/output/single/output.json")
    val result = mutableListOf<Entry>()
    for(file in jsonFiles) {
        val entries = mapper.readValue<List<Entry>>(file)
        entries.forEach{ entry -> result.add(entry) }
    }

    mapper.writeValue(finalJson, result)

    if(finalJson.exists()) {
        val entries: List<Entry> = mapper.readValue(finalJson)
        val entriesGrouped = entries.groupBy { entry -> entry.country }

        for((key, value) in entriesGrouped) {
            val toFile = File("$PROJECT_PATH/src/main/resources/output/multiple/$key.json")
            if(toFile.createNewFile()) {
                mapper.writeValue(toFile, value)
            }
        }
    }
}